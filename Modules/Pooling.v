`timescale 1ns / 1ps

`define LOW 0
`define HIGH 1

/* 
The module decreases the resolution of the input image by calculating the mean of every
(not overlapping) 2x2 pixel segments and producing a new image. 
- Synchronous module
 */
module Pooling(
    input                                   clk,        // System clock
    input                                   rst,        // Reset signal
    input                                   start,      // The module starts processing on the posedge of this signal
    output                                  ready,      // High ready signal shows that the module finished processing
    input   [DATA_WIDTH - 1         : 0]    dataIn,     // Input data
    output  [IN_ADDRESS_WIDTH - 1   : 0]    addressIn,  // Address of the input data
    output  [DATA_WIDTH - 1         : 0]    dataOut,    // Output data
    output  reg [OUT_ADDRESS_WIDTH - 1 : 0] addressOut, // Address of the output data
    output                                  writeEnable // Write enable signal for the output
    );
 
    /* Module configuration parameters */
    parameter IMAGE_WIDTH       = 10;   // Width of the image in pixels
    parameter IMAGE_HEIGHT      = 10;   // Height of the image in pixels
    parameter DATA_WIDTH        = 8;    // Input and output data width in bits
    parameter IN_ADDRESS_WIDTH  = 8;    // Input adderss width in bits
    parameter OUT_ADDRESS_WIDTH = 8;    // Output adderss width in bits
       
    /* Local parameters */
    localparam IDLE_STATE = 0;
    localparam RUNNING_STATE = 1;
    localparam POOLING_DIMENSION = 2;    // Size of the quadratic pooling matrix in pixels
    localparam SUMM_SIZE = DATA_WIDTH + 2;
    
    /* The component shall be in IDLE_STATE when waiting for start signal and be in RUNNING_STATE while processing data */
    reg state;              // Internal state of the module
    always @ (posedge clk)
    begin
        if(rst == `HIGH)
            state <= IDLE_STATE;
        else
        begin
            case(state)
                IDLE_STATE:
                begin
                    if(start == `HIGH)
                        state <= RUNNING_STATE;
                end
                RUNNING_STATE:
                begin
                    if(ready == `HIGH)
                        state <= IDLE_STATE;
                end
            endcase
        end
    end
       
    /* 
    When the buffer is filled the first value will be overridden in the next cycle because the counter keeps counting.
    Simultaneously the mean will be calculated with the correct data, so this is not an error.
    */
    reg [DATA_WIDTH - 1 : 0] inputBuffer [POOLING_DIMENSION*POOLING_DIMENSION - 1 : 0]; // Contains those few pixels that will be pooled
    always @ (posedge clk)
    begin
        inputBuffer[addressIn[$clog2(POOLING_DIMENSION*POOLING_DIMENSION - 1) : 0]] <= dataIn;
    end
    
    /* The inputSumm shall contain the summ of pixels that are currently under the scope of pooling */
    reg [SUMM_SIZE : 0] inputSumm;
    always @ (posedge clk)
    begin
        if(state == IDLE_STATE)
            inputSumm <= 0;
        else
        begin
            if(writeEnable == `HIGH)
                inputSumm <= dataIn;
            else
                inputSumm <= inputSumm + dataIn;
        end
    end
    
    /* Horizontal coordinate of the memory */
    reg [$clog2(IMAGE_WIDTH) : 0] xCoordinate;
    always @ (posedge clk)
    begin
        if(state == IDLE_STATE)
            xCoordinate <= 0;
        else
        begin
            if(xCoordinate == IMAGE_WIDTH - 1)
                xCoordinate <= 0;
            else
            begin
                xCoordinate <= xCoordinate + 1;
            end
        end
    end
    
    /* Vertical coordinate of the memory */
    reg [$clog2(IMAGE_HEIGHT) : 0] yCoordinate;
    always @ (posedge clk)
    begin
        if(state == IDLE_STATE)
            yCoordinate <= 0;
        else
        begin
            if(yCoordinate == IMAGE_HEIGHT)
                yCoordinate <= 0;
            else
            begin
                if(xCoordinate == IMAGE_WIDTH - 1)
                    yCoordinate <= yCoordinate + 1;
            end
        end
    end
    
    /* Counter from 0 to 3 that counts the steps of the pooling cycle */
    reg [$clog2(POOLING_DIMENSION * POOLING_DIMENSION) : 0] poolingStep;
    always @ (posedge clk)
    begin
        if(state == IDLE_STATE)
            poolingStep <= 0;
        else
            poolingStep <= poolingStep + 1;
    end
    
    /* Address of the next pixel in the output memory */
    always @ (posedge clk)
    begin
        if(state == IDLE_STATE)
            addressOut <= 0;
        else
        begin
            if(writeEnable)
                addressOut <= addressOut + 1;
        end
    end
    
    /* The summ is divided by four to calculate the average */
    assign dataOut = inputSumm >> POOLING_DIMENSION;
    
    /* Write enable shall be high when the summ is calculated. It should not be high in the first cycle */
    assign writeEnable = (addressIn[1:0] == 2'b00) && (xCoordinate + yCoordinate != 0);
    
    /* Address of the next pixel in the input memory */
    assign addressIn = yCoordinate * IMAGE_WIDTH + xCoordinate;
    
    /* Ready signal marking that the module finished the pooling of the input memory */
    assign ready = yCoordinate == IMAGE_WIDTH;
    
endmodule
