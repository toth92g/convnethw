`timescale 1ns / 1ps

/* 
Activation function(Sigmoid) y = 1 / (1 + e^(-x))
- Asynchronous module
 */
module NonLinearity(
    input  [DATA_WIDTH - 1 : 0] dataIn,
    output [DATA_WIDTH - 1 : 0] dataOut
    );
    
    /* Module configuration parameters */
    parameter DATA_WIDTH = 8;   // Input and output data width in bits
    
    reg [DATA_WIDTH - 1 : 0] LUT [2**DATA_WIDTH - 1 : 0];
    
    /* Initializing the LUT */
    integer i;
    initial begin
      for (i=0;i<=2**DATA_WIDTH;i=i+1)
        LUT[i] = i;
    end
    
    assign dataOut = LUT[dataIn];

endmodule
