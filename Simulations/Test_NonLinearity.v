`timescale 1ns / 1ps

`define ASSERT(ID, EXPECTED, RESULT) if(EXPECTED != RESULT) $display("ID:%d ", ID, "Expected:%d ", EXPECTED, "Result:%d", RESULT);

module Test_NonLinearity();
    reg [3:0] dataIn;
    wire [3:0] dataOut;
    reg clk;
    
    /* Module under test */
    NonLinearity #(
    // Parameters
    .DATA_WIDTH(4))
    MUT(
    // Ports
    .dataIn(dataIn),
    .dataOut(dataOut)
    );
    
    /* System clock */
    always #5 clk = ~clk; 
     
    /* Initialize the module and start processing */
    initial
    begin
        clk = 0;
        dataIn = 0;
    end
        
    always @ (posedge clk)
        dataIn <= dataIn + 1;
    
    /* Checking if the module is processing the inputs correctly */
    always @ (posedge clk)
    begin
        `ASSERT(dataIn, dataIn, dataOut);
    end
endmodule
