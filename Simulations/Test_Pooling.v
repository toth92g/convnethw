`timescale 1ns / 1ps

`define ASSERT(ID, EXPECTED, RESULT) if(EXPECTED != RESULT) $display("ID:%d ", ID, "Expected:%d ", EXPECTED, "Result:%d", RESULT);

module Test_Pooling();

    reg clk;
    reg rst;
    reg start;
    wire ready;
    reg [7:0] inputVector [15:0];
    reg [7:0] expectedOutputVector [3:0];
    wire [7:0] dataIn;
    wire [3:0] addressIn;
    wire [7:0] dataOut;
    wire [3:0] addressOut;
    wire writeEnable;

    /* Module under test */
    Pooling #(
    // Parameters
    .IMAGE_WIDTH(4),
    .IMAGE_HEIGHT(4),
    .DATA_WIDTH(8),
    .IN_ADDRESS_WIDTH(4),
    .OUT_ADDRESS_WIDTH(4))
    MUT(
    // Ports
    .clk(clk),
    .rst(rst), 
    .start(start),
    .ready(ready),
    .dataIn(dataIn),
    .addressIn(addressIn),
    .dataOut(dataOut),
    .addressOut(addressOut),
    .writeEnable(writeEnable)
    );
    
    /* Initializing the input and output vectors */
    initial
    begin
        inputVector[0] = 0;
        inputVector[1] = 42;
        inputVector[2] = 87;
        inputVector[3] = 127;
        inputVector[4] = 152;
        inputVector[5] = 183;
        inputVector[6] = 217;
        inputVector[7] = 255;
        inputVector[8] = 0;
        inputVector[9] = 42;
        inputVector[10] = 87;
        inputVector[11] = 127;
        inputVector[12] = 152;
        inputVector[13] = 183;
        inputVector[14] = 217;
        inputVector[15] = 255;
        
        expectedOutputVector[0] = 65;
        expectedOutputVector[1] = 201;
        expectedOutputVector[2] = 64;
        expectedOutputVector[3] = 201;
    end
    assign dataIn = inputVector[addressIn];
    
    /* Initialize the module and start processing */
    initial
    begin
        clk = 0;
        start = 0;
        rst = 1;
        #15 rst = 0;
        #10 start = 1;
        #10 start = 0;
    end
    
    /* System clock */
    always #5 clk = ~clk; 
    
    /* Checking if the module is processing the inputs correctly */
    always @ (posedge clk)
    begin
        if(writeEnable)
        begin
            `ASSERT(addressOut, expectedOutputVector[addressOut], dataOut);
        end
    end
endmodule
